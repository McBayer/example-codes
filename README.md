## depoficer-parts mappa:
A Depoficer egy raktárkezelő program, melynek segítségével az adminok karban tarthatják 
a fiókjukhoz tartozó termékeket, a termékek kategóriáit, a landing oldalakat, különféle 
táblázatokat, valamint a partnereik adatait is.
A mappában a Dashboard és a Landing oldal kezelő felületek Vue.js kódja és a hozzájuk tartozó template található.


## sass mappa:
Egy összetett fakereskedő weboldalhoz tartozó sass file-ok egy része.


## survey mappa:
Egy lead generáló oldal, ahol az érdeklődőnek néhány kérdésre kell választ adnia egy űrlapon,
ahol minden egyes kérdés külön boxban jelenik meg, miután válaszolt az előző kérdésre.
Végül meg kell adnia a kapcsolati adatait, amit egy PHP script állít összes és curl segítéségvel
továbbít a leadkezelő adatbázisba.





