<?php
class SQL_Connection {
	private static $sql;
	private $host   	= '***';
	private $db     	= "***";
	private $connev	= "***";
	private $pass   	= '***';	
	private $connection;
	private $site 		= "";
	private $file 		= 'sql_error.log';
	private $query_file = 'sql_query_list.log';
	private $log_cnt = 1;
	public $table;
	
	private function __construct($site = "") {
		$this->connect();
		$this->site = $site;
		$this->generate_table_names();
	}
	
	static function instance() {
		if (!isset(self::$sql)) {
			$class 		= __CLASS__;
			self::$sql 	= new $class;
		}
		return self::$sql;
	}
	
	function connect() {
		$this->connection = mysqli_connect($this->host ,$this->connev, $this->pass);
		if(!$this->connection)
		{
			echo "Sikertelen connect";
		}
		$this->query("SET NAMES 'utf8' collation 'utf8_unicode_ci'");
		mysqli_set_charset($this->connection, "utf8");
		mysqli_select_db($this->connection, $this->db);
	}
	
	function close(){
            mysqli_close($this->connection);
	}
	
	protected function get_query($query) {

		if (!empty($this->connection)) {
			$this->log_query($query);
			$result_sql = mysqli_query($this->connection, $query);
			return $result_sql;
		} else return false;
	}
	
	function query($query){
	   return $this->get_query($query);
	}	
	
	function select_query($query){
		$result_sql = $this->get_query($query);
		if (is_object($result_sql)) {
			$result = mysqli_fetch_object($result_sql);
			mysqli_free_result($result_sql);
		} else {
			$this->log_error($query);
		}
		return $result;
	}
	
	function select_array($query) {
		$result_sql = $this->get_query($query);
		if (is_object($result_sql)) {
			$result = array();
			while($result[] = mysqli_fetch_object($result_sql)) {}
			$resultl = array_pop($result);
			mysqli_free_result($result_sql);
		} else {
			$this->log_error($query);
		}
		return $result;
	}	
	
	function select_value($query) {
		$result_sql = $this->get_query($query);
		if (is_object($result_sql)) {
			list($result) = mysqli_fetch_row($result_sql);
			mysqli_free_result($result_sql);
		} else {
			$this->log_error($query);
		}
		return $result;
	}
	function escaping($object, $html=true) {
		$object = mysqli_real_escape_string($this->connection, $object);
		if ($html)
			$object = htmlspecialchars($object);
		return $object;
	}
	function str2sql($data, $need_special_chars=true) {
		if (!is_numeric($data) && empty($data)) return 'null';
		return "'".$this->escaping($data, $need_special_chars)."'";
	}
	
	protected function log_error($query) {
		$f = fopen($this->file, 'a');
		fwrite($f, date("Y-m-d H:i:s") . ': ' . $query . "\n");
		fwrite($f, 'query error: ' . mysqli_error($this->connection) . "\n");
		fclose($f); 
	}

	protected function log_query($query) {
		$f = fopen($this->query_file, 'a');
		fwrite($f, $this->log_cnt . ". " . date("Y-m-d H:i:s") . ': ' . microtime() . '|' . $query . "|\n");
		fclose($f);
		$this->log_cnt++; 
	}	
	
	public function get_error() {
		return mysqli_error($this->connection);
	}
	
	function get_cache_key($query) {
		return hash_hmac("sha256", $query, "********");
	}	
	
	function delete_from_cache($query) {
		$cache_key = $this->cache_pre . $this->get_cache_key($query);
		$this->log_query($cache_key);
		$this->log_query($query);
	}	
		
	function generate_table_names() {
		$this->table = new StdClass();
		$pre = '***_';

	}	

}

?>
