<?php
session_start();
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';

DEFINE("ADMIN_EMAIL", "***");

// For mail send
DEFINE("MAILSEND_USERNAME", "***");
DEFINE("MAILSEND_PASSWORD", '***');
DEFINE("MAILSEND_HOST", "***");
DEFINE("MAILSEND_PORT", '***');
DEFINE("MAILSEND_SENDER_EMAIL", '***');
DEFINE("MAILSEND_SENDER_NAME", '***');

DEFINE("TEST_EMAIL_1", '***');
DEFINE("TEST_EMAIL_2", '***');
DEFINE("DEFAULT_MAIL_ADDRESS_IN_MAIL", '***');

function depo_send_mail($email, $subject, $message_body, $reply_to = null, $sender = null) {

	$mail = new PHPMailer(true);
	$note = '';
	try {
			//Server settings
			$mail->SMTPDebug = 0;
			$mail->isSMTP();
			$mail->Host       = MAILSEND_HOST;
			$mail->SMTPAuth   = true;
			$mail->Username   = MAILSEND_USERNAME;
			$mail->Password   = MAILSEND_PASSWORD;
			$mail->SMTPSecure = 'ssl';
			$mail->Port       = MAILSEND_PORT;
			$mail->setFrom(MAILSEND_SENDER_EMAIL, MAILSEND_SENDER_NAME);
			$mail->addAddress($email);
			if ($reply_to) {
				if (is_array($reply_to)) {
					$mail->addReplyTo($reply_to["mail"], $reply_to["name"]);
				} else {
					$mail->addReplyTo($reply_to, MAILSEND_SENDER_NAME);
				}
			} else {
				$mail->addReplyTo(MAILSEND_SENDER_EMAIL, MAILSEND_SENDER_NAME);
			}

			$mail->CharSet = 'UTF-8';
			$mail->Encoding = 'base64';

			// Content
			$mail->isHTML(true);
			$mail->Subject = $subject;
			$mail->Body    = $message_body;
			$mail->AltBody = strip_tags($message_body);

			$mail->send();
			$note = 'Message has been sent';
	} catch (Exception $e) {
			$note = "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
	}

	return $note;
}

/**
 *
 */
function getSQLSecretKey($data) {
	return hash_hmac('sha256', $data, '***');
}

/**
 *
 */
function getEncryptedDataSQL($data, $key) {
	global $sql;
	return "***";
}


function checkPhone($phone) {
	$mod_phone = preg_replace('/[^0-9]/', '', $phone);
	$ok_format = true;

	$prenum = substr($mod_phone, 0, 2);

	$prenumber = str_split($prenum);

	if ($prenumber[0] < 2 && $prenumber[1] < 1) {
		$ok_format = false;
	}

	// count phone number part
	$dialer = str_split(substr($mod_phone, 2));
	$phone_nums = [];
	foreach($dialer as $num) {
		if (!isset($phone_nums[$num])) {
			$phone_nums[$num] = 0;
		}
		$phone_nums[$num]++;
	}

	// at least three different number neccessary
	if (count($phone_nums) < 2) {
		$ok_format = false;
	}

	if (strlen($mod_phone) < 8 || strlen($mod_phone) > 13) {
		$ok_format = false;
	}
	return $ok_format;
}

function checkEmail($mail) {
	$token = '***';
	$url = '***/api/check/email?email=' . $mail;
	$curl = curl_init($url);
	curl_setopt ($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_HEADER, false);
	@curl_setopt($curl,CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
	curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		'Api-Token:' . $token,
	));
	$generator_data = curl_exec($curl);
	curl_close($curl);
	$data = json_decode($generator_data);

	return (is_object($data) ? $data->is_valid : false);
}



function getZipCity ($zip) {
	/*******/
	return $city;
}

function is_valid_name($name) {
	$hun_names = [''] /*********/;
	return (in_array($name, $hun_names));
}
