<?php
session_start();
$base_name = pathinfo($_SERVER["PHP_SELF"]);
$base_url = '//' . $_SERVER["HTTP_HOST"] . $base_name["dirname"];
$domain = preg_replace('/\/\/(.*?)(\/|)$/', '$1', $base_url);
if (isset($_GET["utm_source"])) {
	$_SESSION["query_params"] = $_GET;
	$_SESSION["request_data"] = $_REQUEST;
}

/**
 * For testing purpose, not load the gtm
 */
if (isset($_REQUEST["ttt"])) {
	$_SESSION["test_run"] = true;
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Flyersuit Survey</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="/assets/images/favicon.ico" async>
  	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  	<meta http-equiv="cache-control" content="max-age=72000" />
  	<meta http-equiv="cache-control" content="public" />
  	<meta http-equiv="expires" content="72000" />
  	<meta http-equiv="expires" content="Mon, 01 Jan 2024 1:00:00 GMT" />
		<script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script>
    <style type="text/css">
  		.preloader {
  			position: fixed;
  			top: 0;
  			left: 0;
  			width: 100%;
  			height: 100%;
  			background: #232427;
  			z-index: 10000;
  		}
  		.preloader img {
  			display: block;
  			position: relative;
  			left: 50%;
  			top: 50%;
  			width: 50px;
  			height: 50px;
  			margin: -25px 0 0 -25px;
  		}
  	</style>

  	<script>
      /*| dump*/
      var tmaxDataLayer = {};
      var GLOBAL_VARIABLES = {
          'vertical': 'propr',
          'site': '<?php echo $domain; ?>',
          'leadid_campaign': 'propr_lead'
      };
      var baseUrl = '<?php echo $base_url; ?>';
    </script>
  </head>
  <body>
    <noscript>
      <strong>We‘re sorry but <?php echo $domain; ?> doesn‘t work properly without JavaScript enabled. Please enable it to continue.</strong>
    </noscript>
		<?php if (!isset($_SESSION["test_run"]) || !$_SESSION["test_run"]) { ?>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=xxxxxxxxxxxxxx"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		<?php } ?>



    <div id="headerTop"></div>
    <div id="app"></div>

    <?php include("partials/app.html"); ?>
    <?php include("partials/subpage.html"); ?>
    <?php include("partials/contact-page.html"); ?>    
    <?php include("partials/thank-you.html"); ?>
    <?php include("partials/preloader.html"); ?>
    <?php include("partials/progressBar.html"); ?>
    <?php include("partials/unsub.html"); ?>
    <?php include("partials/sorry.html"); ?>


	<script src="<?php echo $base_url; ?>/assets/js/combined.min.js?v=1.1" defer></script>
	<script src="<?php echo $base_url; ?>/assets/js/script.min.js?v=1.1" defer></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/vue-router/3.0.1/vue-router.min.js" async></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/vue-resource/1.5.0/vue-resource.min.js" async></script>
	<script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script>

	<?php if (!isset($_SESSION["test_run"]) || !$_SESSION["test_run"]) { ?>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-xxxxxxxx');</script>
	<!-- End Google Tag Manager -->
	<?php } ?>

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="preload" href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;700&display=swap" as="style">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;700&display=swap">
    <link rel="stylesheet" href="<?php echo $base_url; ?>/assets/style/main.css" media="all">
  </body>
</html>
