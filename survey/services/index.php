<?php

include("../phplib/functions.php");
include("../conversion-api.php");
$pathinfo = explode("/", $_REQUEST["url"]);

$is_valid = false;
$response = ['state' => $is_valid];

switch($pathinfo[0]) {
	case "zipcode":
		$zip = (int)$pathinfo[1];
		
		if (preg_match("/^([0-9]{5})(-[0-9]{4})?$/i", $zip)) {
			$is_valid = true;
			$response["city"] = "";
		}
	break;
	case "validate-email":
		$mail = trim($pathinfo[1]);
		$is_valid = checkEmail($mail);
	break;
	case "validate-phone":
		$phone = trim($pathinfo[1]);
		$is_valid = checkPhone($phone);
	break;
	case "fb-conversion":
		$event_name = $_REQUEST["event"];
		$event_id = '***' . $event_name . '2021';
		$event_source_url = $_REQUEST["conversion_url"];
		
		if (strlen($event_source_url) < 5) {
			$event_source_url = '***';
		}
		
		$lead = [];
		
		if (isset($_REQUEST["email"])) {
			$lead["email"] = $_REQUEST["email"];
		}
		
		if (isset($_REQUEST["full_name"])) {
			$name_parts = explode(" ", $_REQUEST["full_name"]);
			$last_name = $name_parts[0];
			unset($name_parts[0]);
			$first_name = join(" ", $name_parts);
			$lead["last_name"] = $last_name;
			$lead["first_name"] = $first_name;
			
		}
		
		$response["state"] = true;
		$is_valid = true;
		
		$api = new LN_Conversion_API($pixel_id, $access_token);
		$api->setEventSource($event_source_url);
		$api->setEventType($event_name, $event_id);
		
		if (isset($lead["email"]) && $lead["email"]) {
			$api->setLeadData($lead);
		}
		
		$fb_response = $api->sendData();
		$response["fb_response"] = $fb_response;
		
	break;
	
	case "unsub":
		$mail = trim($_REQUEST["mail"]);
		$uid = trim($_REQUEST["uid"]);

		$user_data = [];
		$user_data["from"] = "***";
		$user_data["project_id"] = "***";
		$user_data["mail"] = $mail;
		$user_data["uid"] = $uid;

		$url = "***/delete-lead.php";
		$curl = curl_init($url);
		curl_setopt ($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HEADER, false);
		@curl_setopt($curl,CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($user_data));
		$generator_data = curl_exec($curl);
		curl_close($curl);
		$curl_response_data = json_decode($generator_data);

		$response["generator_data"] = $generator_data;


	break;
	case "form-post":

		$name = trim($_REQUEST["fullname"]);
		$zip = (isset($_REQUEST["zip"]) ? $_REQUEST["zip"] : "");
		$params = $_REQUEST;

		$errors = [];
		$full_name = trim($params["user"]["username"]);
		$name_parts = explode(" ", $full_name);


		if ($params["user"]["mail"] && !checkEmail($params["user"]["mail"])) {
			$errors["mail"] = "Please, check your email adress!";
		}
		if (!$name) {
			$errors["fullname"] = "Wrong name!";
		}

		if (!$params["user"]["country"]) {
			$errors["country"] = "Please add your country!";
		}

		if (!$params["user"]["acceptor"]) {
			$errors["acceptor"] = "Please accept terms and conditions!";
		}

		$response = ["state" => true];

		if (count($errors)) {
			$response["state"] = false;
			$response["errors"] = $errors;
		} else {
			$user_data = $params;
			$user_data["from"] = "***";
			$user_data["project_id"] = "***";
			$cookier_visitor_id = $_REQUEST["visitor_id"];
			$user_data["cookier_visitor_id"] = $cookier_visitor_id;

			// get the lead number
			$lead_nums = @file_get_contents("lead_numbers.txt");
			list($lead_num, $total_lead) = explode(";", $lead_nums);
			$lead_num = (int)$lead_num;
			$total_lead = (int)$total_lead;
			if (!$total_lead) {
				$total_lead = '-';
			}

			$is_test = true;
			$today = date("Y-m-d");
			$daily_leads = [];
			if (!in_array($user_data["user"]["mail"], ["***"])) {
				$lead_num++;
				file_put_contents("lead_numbers.txt", $lead_num . ';' . $total_lead);
				$is_test = false;

				$lead_csv = "daily_lead_numbers.csv";
				if (($handle = fopen($lead_csv, "r")) !== FALSE) {
					while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
							$daily_leads[$data[0]] = $data[1];
					}
					fclose($handle);
					if (!isset($daily_leads[$today])) {
						$daily_leads[$today] = 1;
					} else {
						$daily_leads[$today] += 1;
					}

					$lfp = fopen($_SERVER["DOCUMENT_ROOT"] . '/' . $lead_csv, "a");
					foreach($daily_leads as $day => $value) {
						fwrite($lfp, $day . ";" . (int)$value . "\n");
					}
					fclose($lfp);
				}
			}

			$user_data["is_test"] = ($is_test ? 1 : 0);
			
			// Save the lead
			$url = "***/save-lead.php";
			$curl = curl_init($url);
			curl_setopt ($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_HEADER, false);
			@curl_setopt($curl,CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($curl, CURLOPT_POST, 1);
			curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($user_data));
			$generator_data = curl_exec($curl);
			curl_close($curl);
			$curl_response_data = json_decode($generator_data);


			/*************************************************************/
			$event_name = "Lead";
			$event_id = '***Lead2021';
			$event_source_url = '***/survey/thanks';

			$name_parts = explode(" ", $name);
			$last_name = $name_parts[0];
			unset($name_parts[0]);
			$first_name = join(" ", $name_parts);
			
			$lead = [
				"email" => $params["user"]["mail"],
				"last_name" => $last_name,
				"first_name" => $first_name,
			];

			$api = new LN_Conversion_API($pixel_id, $access_token);
			$api->setEventSource($event_source_url);
			$api->setEventType($event_name, $event_id);
			if (isset($lead["email"]) && $lead["email"]) {
				$api->setLeadData($lead);
			}
			$api_response = $api->sendData();
			/*************************************************************/



			if ($is_test) {
				$subject = '*** tesztlead érkezett';
			} else {
				$subject = '*** lead (' . $daily_leads[$today] . '/' . $lead_num . '/' . (is_numeric($total_lead) ? $total_lead - $lead_num : $total_lead) . ')';
			}
			$udata = $user_data["user"];
			$message_body = '***';

			$dev_message = '***';


			$lead_admins = ["***"];
			$dev_admins = ["***"];
			if ($is_test) {
				$lead_admins = ["***"];
			}

			foreach($lead_admins as $email) {
				$note = depo_send_mail($email, $subject, $message_body);
			}
			foreach($dev_admins as $email) {
				$note = depo_send_mail($email, $subject, $dev_message);
			}

			$subject = 'Thank you for your answers!';
			$user_message_body = '
			<table align="center" border="0" cellpadding="0" cellspacing="0" style="font-family: Verdana, sans-serif;background-color:#FFF;border-collapse:collapse;max-width:600px;width:100%;margin:0 auto;">
			    <tr>
			     <td style="padding:10px;background-color:#000;">
			       <img src="https://flyersuit.com/assets/images/logo.png" alt="Flyersuit" style="max-width:150px;width:100%;margin:0 auto;display:block;">
			     </td>
			    </tr>
			    <tr>
			     <td style="background-color:#0F7499;color:#0c0e24;text-align:center;">
			      <p style="font-size:16px;font-weight:700;margin:15px 0;color:#FFF;">We got your answers</p>
			     </td>
			    </tr>
			    ***
			  </table>
			';
			
			try {
				$note = depo_send_mail($udata["mail"], $subject, $user_message_body);

				$admin_message_body = '***';
				$note = depo_send_mail("***", "***", $admin_message_body);
			
				if ($curl_response_data->uid) {
					$response["uid"] = $curl_response_data->uid;
					$response["note"] = $note;
					$is_valid = true;
				}
			} catch (Exception $e) {
				$is_valid = false;
				$response["note"] = "Error during the mail send...";
			}
		}

	break;
}
$response["state"] = $is_valid;

echo json_encode($response);
